package com.kshrd.movieapp.Repo;

/*
 * Copyright (C) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.kshrd.movieapp.model.Category;
import com.kshrd.movieapp.viewmodels.CategoryDao;

import java.util.List;

/**
 * Abstracted Repository as promoted by the Architecture Guide.
 * https://developer.android.com/topic/libraries/architecture/guide.html
 */

class MovieRepository {

    private CategoryDao mCategoryDao;
    private LiveData<List<Category>> mAllCategory;

    MovieRepository(Application application) {
        WordRoomDatabase db = WordRoomDatabase.getDatabase(application);
        mCategoryDao = db.wordDao();
        mAllCategory = mCategoryDao.getAlphabetizedWords();
    }

    LiveData<List<Category>> getAllWords() {
        return mAllCategory;
    }

}

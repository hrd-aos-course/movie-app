package com.kshrd.movieapp.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "category")
public class Category {
    @PrimaryKey public long category_id;
    public String name;
    public String image;

    public Category(long category_id, String name, String image) {
        this.category_id = category_id;
        this.name = name;
        this.image = image;
    }

    public long getCategory_id() {
        return category_id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }
}

@Entity(tableName = "movie")
class Movie {
    @PrimaryKey(autoGenerate = true)
    private long id_movie;
    private long category_id_fk;
    private String title;
    private String image;

    public Movie(long id_movie, long category_id_fk, String title, String image) {
        this.id_movie = id_movie;
        this.category_id_fk = category_id_fk;
        this.title = title;
        this.image = image;
    }

    public long getId_movie() {
        return id_movie;
    }

    public long getCategory_id_fk() {
        return category_id_fk;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }
}







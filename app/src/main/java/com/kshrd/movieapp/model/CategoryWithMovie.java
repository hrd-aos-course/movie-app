package com.kshrd.movieapp.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class CategoryWithMovie {

    @Embedded
    public Category category;
    @Relation(
            parentColumn = "category_id",
            entityColumn = "category_id_fk"
    )
    public List<Movie> movieList;

}

package com.kshrd.movieapp.viewmodels;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.kshrd.movieapp.model.Category;
import com.kshrd.movieapp.model.CategoryWithMovie;

import java.util.List;

@Dao
public interface CategoryDao {
    @Transaction
    @Query("SELECT * FROM category")
    public List<CategoryWithMovie> getCategoryWithMovie();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Category category);
}
